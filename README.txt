This is a very simple module which allows the administrators to restrict the logins during site updates. The access can be restricted per role.

Install & configure No Access module:
1. Navigate to administer >> build >> modules. Enable No Accees module (under Other fieldgroup).
2. Change the settings at: administer >> settings >> noacces